<!DOCTYPE html >
  <head>
  </head>

  <body>
	<div id="content"></div>
    <script>
        function readEvents(marker_location, anEvent) {
          var allEvents = [];

            // Change this depending on the name of your PHP or XML file
            downloadUrl('badder.xml', function(data) {
                var xml = data.responseXML;
                //var events = xml.documentElement.getElementsByTagName('item');
                var events = xml.documentElement.getElementsByTagName('channel')[0].children;
                //var events = channel.getElementsByTagName('item');
                var skip = true;

                Array.prototype.forEach.call(events, function(itemElem) {
                    if(itemElem.children.length>0) {
                      if (skip) {
                        skip=false;
                      }
                      else {
                        var title = itemElem.getElementsByTagName('title')[0].innerHTML;
                        var description = itemElem.getElementsByTagName('description')[0].innerHTML;
                        var link = itemElem.getElementsByTagName('link')[0].innerHTML;
                        var location = itemElem.getElementsByTagName('location')[0].innerHTML;


                        if(location === marker_location) {
                          var eventObject = {
                            Title: title,
                            Description: description,
                            Link: link
                          };
                          anEvent.Title = title;
                          anEvent.Description = description;
                          anEvent.Link = link;
                          //console.log(Object.values(anEvent));
                          allEvents.push(eventObject);
                          // console.log(allEvents);
                        }


                      }
                    }
                });
            });
            //console.log(allEvents);
          return allEvents;
        }



      function downloadUrl(url, callback) {
        var request = window.ActiveXObject ?
            new ActiveXObject('Microsoft.XMLHTTP') :
            new XMLHttpRequest;

        request.onreadystatechange = function() {
          if (request.readyState == 4) {
            request.onreadystatechange = doNothing;
            callback(request, request.status);
          }
        };

        request.open('GET', url, true);
        request.send(null);
      }

      function doNothing() {}
      //console.log(readEvents("South Bank Parklands"));
    </script>
  </body>
</html>