<!DOCTYPE html >
    <head>
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
        <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
        <title>Brisbane Events - Poppin' Off</title>
    </head>

    <body>
        <div class="wrapper">
            <div class="masthead">
            </div>
            
            <div class="content">
                <?php
                    if(isset($_POST['range'])) {
                        $rangeMax = $_POST['range'];
                    }
                    else {
                        $rangeMax = 100;
                    }
                    if(isset($_POST['eventType'])) {
                        $eventType = $_POST['eventType'];
                    } else {
                        $eventType = "";
                    }
                ?>
                <form id="range" action="bigju.php" method="post">
                    <label for="range">Range:</label>
                    <input type="range" name="range" id="rangeSlider" value="<?php echo($rangeMax) ?>" oninput="updateRange()" />
                    <span id="rangeIndicator" style="width:20%">0-10km</span>
                    <label for="eventType">Event Type:</label>
                    <input id="EventType" type="text" name="eventType" value="<?php echo($eventType); ?>"/>
                    <script>
                        updateRange();
                    </script>
                    <input type="submit" id="submit_button" name="Submit" value="Go!" />
                </form>
                <div id="map"></div>
                <script>
                    function updateRange() {
                        document.getElementById('rangeIndicator').innerHTML="0-"+document.getElementById('rangeSlider').value/10+"km";
                    }
                    function initMap() {
                        var map = new google.maps.Map(document.getElementById('map'), {
                            zoom: 10,
                            center: {lat: -27.45556, lng: 152.94166}
                        });
                        var infoWindow = new google.maps.InfoWindow;

                        if (navigator.geolocation) {
                            navigator.geolocation.getCurrentPosition(function(position) {
                                var pos = {
                                    lat: position.coords.latitude,
                                    lng: position.coords.longitude
                                };

                                infoWindow.setPosition(pos);
                                infoWindow.setContent('Location found.');
                                infoWindow.open(map);
                                map.setCenter(pos);
                            }, function() {
                                handleLocationError(true, infoWindow, map.getCenter());
                            });
                        } else {
                            // Browser doesn't support Geolocation
                            handleLocationError(false, infoWindow, map.getCenter());
                        }

                        downloadUrl('markers.php', function(data) {
                            var xml = data.responseXML;
                            var markers = xml.documentElement.getElementsByTagName('marker');

                            Array.prototype.forEach.call(markers, function(markerElem) {
                                var id = markerElem.getAttribute('id');
                                var name = markerElem.getAttribute('name');
                                var address = markerElem.getAttribute('address');
                                var type = markerElem.getAttribute('type');
                                var point = new google.maps.LatLng(
                                parseFloat(markerElem.getAttribute('lat')),
                                parseFloat(markerElem.getAttribute('lng')));

                                var infowincontent = document.createElement('div');
                                var strong = document.createElement('strong');
                                strong.textContent = name
                                infowincontent.appendChild(strong);
                                infowincontent.appendChild(document.createElement('br'));

                                var text = document.createElement('text');
                                downloadUrl('badder.xml', function(data) {
                                    var xml = data.responseXML;
                                    var events = xml.documentElement.getElementsByTagName('channel')[0].children;
                                    var skip = true;
                                    var numEvents = 0;
                                    Array.prototype.forEach.call(events, function(itemElem) {
                                        if(itemElem.children.length>0) {
                                            if (skip) {
                                                skip=false;
                                            }
                                            else {
                                                var title = itemElem.getElementsByTagName('title')[0].innerHTML;
                                                var description = itemElem.getElementsByTagName('description')[0].innerHTML;
                                                var eventType1 = itemElem.getElementsByTagName('customfield')[0].innerHTML;
                                                var eventType2 = itemElem.getElementsByTagName('customfield')[1].innerHTML;
                                                var eventType3 = itemElem.getElementsByTagName('customfield')[2].innerHTML;
                                                var link = itemElem.getElementsByTagName('link')[0].innerHTML;
                                                var location = itemElem.getElementsByTagName('location')[0].innerHTML;
                                                
                                                if (location.includes(markerElem.getAttribute('name')) || markerElem.getAttribute('name').contains) {
                                                    numEvents++;
                                                    var text = document.createElement('text');
                                                    text.textContent = title
                                                    infowincontent.appendChild(text);
                                                    infowincontent.appendChild(document.createElement('br'));
                                                    var text2 = document.createElement('text');
                                                    text2.textContent = description
                                                    infowincontent.appendChild(text2);
                                                    infowincontent.appendChild(document.createElement('br'));
                                                    var text3 = document.createElement('text');
                                                    text3.textContent = eventType1
                                                    infowincontent.appendChild(text3);
                                                     infowincontent.appendChild(document.createElement('br'));
                                                     var text4 = document.createElement('text');
                                                    text4.textContent = eventType2
                                                    infowincontent.appendChild(text4);
                                                     infowincontent.appendChild(document.createElement('br'));
                                                     var text5 = document.createElement('text');
                                                    text5.textContent = eventType3
                                                    infowincontent.appendChild(text5);
                                                    infowincontent.appendChild(document.createElement('hr'));
                                                }
                                            }
                                        }
                                    });
                                    var text = document.createElement('text');
                                    text.textContent = "Num. of Events: "+numEvents;
                                    infowincontent.appendChild(text);
                                });
                                var posi = {lat: -27.4989219, lng: 153.0146385};
                                if (calcDistance(posi.lat, posi.lng, parseFloat(markerElem.getAttribute('lat')), parseFloat(markerElem.getAttribute('lng')))<(document.getElementById('rangeSlider').value/10)) {
                                    var marker = new google.maps.Marker({
                                        map: map,
                                        position: point
                                    });
                                    marker.addListener('click', function() {
                                        
                                        infoWindow.setContent(infowincontent);
                                        infoWindow.open(map, marker);
                                    });
                                }
                            });
                        });

                        var height = window.innerHeight-20;
                        height = height.toString()+"px";
                        document.getElementById('map').style.height=height;
                        document.getElementById('map').style.width="100%";
                    }

                    function calcDistance(lat1, lon1, lat2, lon2) {
                        var R = 6371e3; // metres
                        var phi1 = lat1*Math.PI / 180;
                        var phi2 = lat2*Math.PI / 180;
                        var deltaPhi = (lat2-lat1)*Math.PI / 180;
                        var deltaLambda = (lon2-lon1)*Math.PI / 180;

                        var a = Math.sin(deltaPhi/2) * Math.sin(deltaPhi/2) +
                                Math.cos(phi1) * Math.cos(phi2) *
                                Math.sin(deltaLambda/2) * Math.sin(deltaLambda/2);
                        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

                        var d = R * c;
                        d = (d/1000.0).toFixed(2);
                        return d;
                    }

                    function downloadUrl(url, callback) {
                        var request = window.ActiveXObject ?
                        new ActiveXObject('Microsoft.XMLHTTP') :
                        new XMLHttpRequest;

                        request.onreadystatechange = function() {
                            if (request.readyState == 4) {
                                request.onreadystatechange = doNothing;
                                callback(request, request.status);
                            }
                        };

                        request.open('GET', url, true);
                        request.send(null);
                    }

                    function doNothing() {}
                </script>
                
                <script
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB8bFFp9UIGZe8F4vgYf1mbpam2vlAt8N4&callback=initMap">
                </script>
            </div>
        </div>
    </body>
</html>