<!DOCTYPE html >
    <head>
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
        <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
        <title>Brisbane - Poppin' Off</title>
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <script type="text/javascript" src="js/general.js"></script>
    </head>

    <body>
        <div class="wrapper">
            <div class="masthead">
                <?php
                    include "header.php";
                ?>
            </div>
            <div class="content">
                <div id="map"></div>

                <script>

                    var Title = "";
                    var Description = "";
                    var Link = "";

                    var anEvent = {Title: "", Description: "", Link: ""};
                    var allEvents = [];

                    var customLabel = {
                        restaurant: {
                            label: 'R'
                        },
                        bar: {
                            label: 'B'
                        }
                    };

                    function calcDistance(lat1, lon1, lat2, lon2) {
                        var R = 6371e3; // metres
                        var phi1 = lat1*Math.PI / 180;
                        var phi2 = lat2*Math.PI / 180;
                        var deltaPhi = (lat2-lat1)*Math.PI / 180;
                        var deltaLambda = (lon2-lon1)*Math.PI / 180;

                        var a = Math.sin(deltaPhi/2) * Math.sin(deltaPhi/2) +
                                Math.cos(phi1) * Math.cos(phi2) *
                                Math.sin(deltaLambda/2) * Math.sin(deltaLambda/2);
                        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

                        var d = R * c;
                        d = (d/1000.0).toFixed(2);
                        return d;
                    }

                      function readEvents(marker_location, anEvent) {

                            // Change this depending on the name of your PHP or XML file
                            downloadUrl('badder.xml', function(data) {
                                var xml = data.responseXML;
                                //var events = xml.documentElement.getElementsByTagName('item');
                                var events = xml.documentElement.getElementsByTagName('channel')[0].children;
                                //var events = channel.getElementsByTagName('item');
                                var skip = true;

                                Array.prototype.forEach.call(events, function(itemElem) {
                                    if(itemElem.children.length>0) {
                                        if (skip) {
                                            skip=false;
                                        }
                                        else {
                                            title = itemElem.getElementsByTagName('title')[0].innerHTML;
                                            description = itemElem.getElementsByTagName('description')[0].innerHTML;
                                            link = itemElem.getElementsByTagName('link')[0].innerHTML;
                                            var location = itemElem.getElementsByTagName('location')[0].innerHTML;

                                            if(location === marker_location) {
                                                var eventObject = {
                                                    Title: title,
                                                    Description: description,
                                                    Link: link
                                                };
                                                
                                                anEvent.Title = title;
                                                anEvent.Description = description;
                                                anEvent.Link = link;
                                                //console.log(Object.values(anEvent));
                                                allEvents.push(eventObject);
                                            }

                                        }
                                    }
                                });

                                console.log(allEvents);
                            });

                            console.log(allEvents);
                            return allEvents;
                            
                        }

                    function initMap() {
                        var map = new google.maps.Map(document.getElementById('map'), {
                            zoom: 10,
                            center: {lat: -27.45556, lng: 152.94166}
                        });
                        var infoWindow = new google.maps.InfoWindow;

                        if (navigator.geolocation) {
                            navigator.geolocation.getCurrentPosition(function(position) {
                                var pos = {
                                    lat: position.coords.latitude,
                                    lng: position.coords.longitude
                                };
                                    infoWindow.setPosition(pos);
                                    infoWindow.setContent('Location found.');
                                    infoWindow.open(map);
                                    map.setCenter(pos);
                            }, function() {
                                handleLocationError(true, infoWindow, map.getCenter());
                            });
                        } else {
                            // Browser doesn't support Geolocation
                            handleLocationError(false, infoWindow, map.getCenter());
                        }

                        // Change this depending on the name of your PHP or XML file
                        downloadUrl('markers.php', function(data) {
                            var xml = data.responseXML;
                            var markers = xml.documentElement.getElementsByTagName('marker');
                            Array.prototype.forEach.call(markers, function(markerElem) {
                                var id = markerElem.getAttribute('id');
                                var name = markerElem.getAttribute('name');
                                var address = markerElem.getAttribute('address');
                                var type = markerElem.getAttribute('type');
                                var point = new google.maps.LatLng(
                                parseFloat(markerElem.getAttribute('lat')),
                                parseFloat(markerElem.getAttribute('lng')));

                                var infowincontent = document.createElement('div');
                                var strong = document.createElement('strong');
                                strong.textContent = name
                                infowincontent.appendChild(strong);
                                infowincontent.appendChild(document.createElement('br'));

                                var text = document.createElement('text');
                                //console.log(name);

                                if(name === "South Bank Parklands"){

                                    var allEvents = readEvents(name, anEvent);
            
                                    console.log(allEvents[0].Title);
                                    text.textContent = allEvents[0].Title;
                                    //console.log(anEvent.Title);
        
                                }

                                var posi = {lat: -27.4989219, lng: 153.0146385};
                                
                                //text.textContent = address                                
                                
                                infowincontent.appendChild(text);
                                var icon = customLabel[type] || {};
                                if (calcDistance(posi.lat, posi.lng, parseFloat(markerElem.getAttribute('lat')), parseFloat(markerElem.getAttribute('lng')))<2.5) {
                                    var marker = new google.maps.Marker({
                                        map: map,
                                        position: point,
                                        label: icon.label
                                    });
                                    marker.addListener('click', function() {
                                        
                                        infoWindow.setContent(infowincontent);
                                        infoWindow.open(map, marker);
                                    });
                                }
                            });
                        });
                        var height = window.innerHeight-20;
                        height = height.toString()+"px"
                        document.getElementById('map').style.height=height;
                        document.getElementById('map').style.width="100%";
                    }

                    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
                        infoWindow.setPosition(pos);
                        infoWindow.setContent(browserHasGeolocation ?
                                            'Error: The Geolocation service failed.' :
                                            'Error: Your browser doesn\'t support geolocation.');
                        infoWindow.open(map);
                    }

                    function downloadUrl(url, callback) {
                        var request = window.ActiveXObject ?
                        new ActiveXObject('Microsoft.XMLHTTP') :
                        new XMLHttpRequest;

                        request.onreadystatechange = function() {
                            if (request.readyState == 4) {
                                request.onreadystatechange = doNothing;
                                callback(request, request.status);
                            }
                        };

                        request.open('GET', url, true);
                        request.send(null);
                    }

                    function doNothing() {}
                </script>
                <script
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB8bFFp9UIGZe8F4vgYf1mbpam2vlAt8N4&callback=initMap">
                </script>
            </div>
            <div class="footer">
            </div>
        </div>
    </body>
</html>