<!DOCTYPE HTML>
<html>
    <head>
        <title>Brisbane Parks 'n' Rec</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<script type="text/javascript" src="js/general.js"></script>
    </head>
    <body>
        <div class="wrapper">
            <div class="masthead">
                <?php
                    include "header.php";
                ?>
            </div>
            <div class="content">
            </div>
            <div class="footer">
            </div>
        </div>
    </body>
</html>